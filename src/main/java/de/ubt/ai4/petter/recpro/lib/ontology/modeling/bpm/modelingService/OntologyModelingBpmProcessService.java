package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.ElementType;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Process;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.ProcessCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@Service
@AllArgsConstructor
public class OntologyModelingBpmProcessService {

    private ModelService modelService;
    private OntologyModelingBpmProcessModelService processModelService;
    private OntologyModelingBpmElementService elementService;

    public void createProcesses(List<Process> processes) {
        processes.forEach(this::createProcess);
    }

    public void createProcess(Process process) {
        OntModel model = modelService.getModel();
        Individual processIndividual = this.createProcessIndividual(process, model);
        processModelService.createProcessModel(process.getProcessModel(), model, processIndividual);
        modelService.writeModel(model);
    }

    public Individual createProcessIndividual(Process process, OntModel model) {
        Individual processIndividual = ModelingService.getIndividual(model, PROCESS, process.getId());
        this.createDataProperties(model, processIndividual, process);
        return processIndividual;
    }

    private void createDataProperties(OntModel model, Individual processIndividual, Process process) {
        elementService.createRecproElementIndividual(process, processIndividual, model);
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_PROCESS_KEY, processIndividual, process.getKey());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_PROCESS_VERSION, processIndividual, process.getVersion());
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_PROCESS_IS_LATEST, processIndividual, String.valueOf(process.isLatest()));
    }

    public List<Process> getAll() {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, PROCESS);
        return ProcessCreatorService.processFromResIterator(individuals, model);
    }

    public List<Process> getLatest() {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, PROCESS);
        ExtendedIterator<Resource> extendedIterator = individuals.filterKeep(
                resource -> resource.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_PROCESS_IS_LATEST))
                && Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_PROCESS_IS_LATEST))
        );

        return ProcessCreatorService.processFromResIterator(extendedIterator, model);
    }

    public Process getById(String processId) {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, PROCESS);
        ExtendedIterator<Resource> extendedIterator = individuals.filterKeep(
                resource -> resource.hasProperty(ModelingService.getPropertyFromModel(model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                && processId.equals(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_ID))
                && ElementType.PROCESS.equals(ElementType.findType(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_TYPE)))
        );
        if (extendedIterator.hasNext()) {
            return ProcessCreatorService.processFromResource(extendedIterator.next(), model);
        } else {
            return new Process();
        }
    }

    public List<Process> getByActivityId(String activityId) {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, PROCESS);

        List<Process> result = new ArrayList<>();

        while (individuals.hasNext()) {
            Resource process = individuals.nextResource();

            ExtendedIterator<Resource> processModelIterator = process.
                    listProperties(ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_HAS_PROCESS_MODEL))
                    .mapWith(Statement::getObject)
                    .filterKeep(RDFNode::isResource)
                    .mapWith(RDFNode::asResource);

            while (processModelIterator.hasNext()) {
                Resource processModel = processModelIterator.next();
                ExtendedIterator<Resource> activityIterator = processModel.listProperties(ModelingService.getPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_MODEL_HAS_ACTIVITY))
                        .mapWith(Statement::getObject)
                        .filterKeep(RDFNode::isResource)
                        .mapWith(RDFNode::asResource);
                while (activityIterator.hasNext()) {
                    Resource activity = activityIterator.next();

                    Statement statement = activity.getProperty(ModelingService.getDatatypeProperty(model, DATA_PROPERTY_RECPRO_ELEMENT_ID));
                    if ((statement != null) && statement.getObject().asLiteral().getLexicalForm().equals(activityId)) {
                        result.add(ProcessCreatorService.processFromResource(process, model));
                    }
                }
            }
        }
        return result;
    }

    public List<Process> getByAttributeId(String attributeId) {
        OntModel model = modelService.getModel();
        ResIterator individuals = ModelingService.getResIterator(model, PROCESS);

        List<Process> result = new ArrayList<>();

        while (individuals.hasNext()) {
            Resource process = individuals.nextResource();

            ExtendedIterator<Resource> attributeIterator = process
                    .listProperties(ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_RECPRO_ELEMENT_HAS_ATTRIBUTE))
                    .mapWith(Statement::getObject)
                    .filterKeep(RDFNode::isResource)
                    .mapWith(RDFNode::asResource);

            if (
                    attributeIterator.filterKeep(resource -> ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_RECPRO_ELEMENT_ID).equals(attributeId)).hasNext()
            ) {
                result.add(ProcessCreatorService.processFromResource(process, model));
                break;
            }
        }
        return result;
    }
}
