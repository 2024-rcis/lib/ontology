package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.ProcessModel;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class OntologyModelingBpmProcessModelService {
    private OntologyModelingBpmElementService elementService;
    private OntologyModelingBpmActivityService activityService;
    private OntologyModelingBpmRoleService roleService;

    public void createProcessModel(ProcessModel processModel, OntModel model, Individual processIndividual) {
        Individual processModelIndividual = ModelingService.getIndividual(model, PROCESS_MODEL, processModel.getId());
        this.createDataProperties(model, processModelIndividual, processModel);
        this.createObjectProperties(model, processIndividual, processModelIndividual);
    }

    private void createDataProperties(OntModel model, Individual processModelIndividual, ProcessModel processModel) {
        elementService.createRecproElementIndividual(processModel, processModelIndividual, model);
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_PROCESS_MODEL_XML, processModelIndividual, processModel.getXml());
        activityService.createActivities(processModel.getActivities(), model, processModelIndividual);
        roleService.createRoles(processModel.getRoles(), model, processModelIndividual, false);
    }

    private void createObjectProperties(OntModel model, Individual processIndividual, Individual processModelIndividual) {
        ObjectProperty processHasProcessModel = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_HAS_PROCESS_MODEL);
        ObjectProperty processModelBelongsToProcess = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_MODEL_BELONGS_TO_PROCESS);

        ModelingService.addProperty(processIndividual, processHasProcessModel, processModelIndividual);
        ModelingService.addProperty(processModelIndividual, processModelBelongsToProcess, processIndividual);
    }
}
