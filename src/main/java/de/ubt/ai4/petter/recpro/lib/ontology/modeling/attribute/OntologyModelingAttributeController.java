package de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/attribute/")
@AllArgsConstructor
public class OntologyModelingAttributeController {

    private AttributeModelingService attributeModelingService;

    @GetMapping("getAll")
    public ResponseEntity<List<RecproAttribute>> getAll() {
        return ResponseEntity.ok(attributeModelingService.getAll());
    }

    @GetMapping("getById/{attributeId}")
    public ResponseEntity<RecproAttribute> getById(@PathVariable String attributeId) {
        return ResponseEntity.ok(attributeModelingService.getById(attributeId));
    }

    @GetMapping("getByIds")
    public ResponseEntity<List<RecproAttribute>> getByIds(@RequestParam List<String> attributeIds) {
        return ResponseEntity.ok(attributeModelingService.getByIds(attributeIds));
    }

    @GetMapping("getByActivityId/{activityId}")
    public ResponseEntity<List<RecproAttribute>> getByActivityId(@PathVariable String activityId) {
        return ResponseEntity.ok(attributeModelingService.getByRecproElementId(activityId));
    }

    @GetMapping("getByActivityIds")
    public ResponseEntity<List<RecproAttribute>> getByActivityIds(@RequestParam List<String> activityIds) {
        return ResponseEntity.ok(attributeModelingService.getByActivityIds(activityIds));
    }

    @PostMapping("createWithElement/{elementId}")
    public void createAttribute(@RequestBody RecproAttribute attribute, @PathVariable String elementId) {
        attributeModelingService.createAttribute(attribute, elementId);
    }

    @PostMapping("createWithElements")
    public void createAttribute(@RequestBody RecproAttribute attribute, @RequestParam List<String> elementIds) {
        attributeModelingService.createAttribute(attribute, elementIds);
    }

    @PostMapping("create")
    public void create(@RequestBody RecproAttribute attribute) {
        attributeModelingService.create(attribute);
    }

    @DeleteMapping("delete/{attributeId}")
    public void delete(@PathVariable String attributeId) {
        attributeModelingService.delete(attributeId);
    }
}
