package de.ubt.ai4.petter.recpro.lib.ontology.modeling;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/modeling/ontology")
@AllArgsConstructor
public class OntologyModelingController {

    private ModelService modelService;
    @GetMapping("/clear")
    public void clear() {
        modelService.clearOntology();
    }
}
