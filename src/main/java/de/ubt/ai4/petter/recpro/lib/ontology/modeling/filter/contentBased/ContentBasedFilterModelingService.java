package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.contentBased;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.ContentBasedFilter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyAttributeIds.ATTRIBUTE;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.OBJECT_PROPERTY_FILTER_USES_ATTRIBUTE;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.OBJECT_PROPERTY_FILTER_USES_RATING;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class ContentBasedFilterModelingService {

    public Individual createFilterIndividual(ContentBasedFilter filter, OntModel model) {
        Individual individual = ModelingService.getIndividual(model, FILTER_CONTENT_BASED, filter.getId());
        this.createDataProperties(model, individual, filter);
        this.createObjectProperties(model, individual, filter);
        return individual;
    }

    private void createDataProperties(OntModel model, Individual individual, ContentBasedFilter filter) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ALL_INPUT_ATTRIBUTES, individual, String.valueOf(filter.isAllInputAttributes()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ALL_INPUT_ELEMENTS, individual, String.valueOf(filter.isAllInputElements()));
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_FILTER_ALL_INPUT_RATINGS, individual, String.valueOf(filter.isAllInputRatings()));
    }

    private void createObjectProperties(OntModel model, Individual filterIndividual, ContentBasedFilter filter) {
        ObjectProperty filterUsesAttribute = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_FILTER_USES_ATTRIBUTE);
        ObjectProperty filterUsesRecproElement = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_FILTER_USES_BPM_ELEMENT);
        ObjectProperty filterUsesRating = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_FILTER_USES_RATING);

        filterIndividual.removeAll(filterUsesAttribute);
        filterIndividual.removeAll(filterUsesRecproElement);
        filterIndividual.removeAll(filterUsesRating);

        if (!filter.isAllInputAttributes()) {
            filter.getAttributes().forEach(attribute -> {
                Individual individual = ModelingService.getIndividual(model, ATTRIBUTE, attribute.getId());
                ModelingService.addProperty(filterIndividual, filterUsesAttribute, individual);
            });
        }

        if (!filter.isAllInputElements()) {
            filter.getBpmElements().forEach(recproElement -> {
                Individual individual = ModelingService.getIndividual(model, RECPRO_ELEMENT, recproElement.getId());
                ModelingService.addProperty(filterIndividual, filterUsesRecproElement, individual);
            });
        }

        if (!filter.isAllInputRatings()) {
            filter.getRatings().forEach(rating -> {
                Individual individual = ModelingService.getIndividual(model, RATING, rating.getId());
                ModelingService.addProperty(filterIndividual, filterUsesRating, individual);
            });
        }
    }
}
