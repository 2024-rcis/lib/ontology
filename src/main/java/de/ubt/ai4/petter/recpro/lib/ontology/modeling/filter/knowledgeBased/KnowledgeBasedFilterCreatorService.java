package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.knowledgeBased;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.KnowledgeBasedFilter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.util.FilterCreatorUtil;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@AllArgsConstructor
@Service
public class KnowledgeBasedFilterCreatorService {

    private FilterCreatorUtil filterCreatorUtil;
    public KnowledgeBasedFilter fromResource(Resource resource, OntModel model) {
        KnowledgeBasedFilter result = new KnowledgeBasedFilter();

        filterCreatorUtil.initialize(result, resource, model);
        result.setAllInputAttributes(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_ATTRIBUTES)));
        result.setAllInputElements(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ALL_INPUT_ELEMENTS)));
        result.setAttributes(filterCreatorUtil.setAttributes(result.isAllInputAttributes(), resource, model));
        result.setBpmElements(filterCreatorUtil.setElements(result.isAllInputElements(), resource, model));
        return result;
    }
}
