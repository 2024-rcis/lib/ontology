package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Rating;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/rating/")
@AllArgsConstructor
public class OntologyModelingRatingController {

    private RatingModelingService ratingModelingService;

    @GetMapping("getAll")
    public ResponseEntity<List<Rating>> getAll() {
        return ResponseEntity.ok(ratingModelingService.getAll());
    }

    @GetMapping("getById/{ratingId}")
    public ResponseEntity<Rating> getById(@PathVariable String ratingId) {
        return ResponseEntity.ok(ratingModelingService.getRatingById(ratingId));
    }

    @PostMapping("create")
    public void createRating(@RequestBody Rating rating) {
        ratingModelingService.createRating(rating);
    }

    @GetMapping("getByBpmElementId/{bpmElementId}")
    public ResponseEntity<Rating> getByBpmElementId(@PathVariable String bpmElementId) {
        return ResponseEntity.ok(ratingModelingService.getByBpmElementId(bpmElementId));
    }

    @GetMapping("addToBpmElement")
    public void addToBpmElement(@RequestParam String bpmElementId, @RequestParam String ratingId) {
        ratingModelingService.addRatingToBpmElement(ratingId, bpmElementId);
    }

    @GetMapping("getDefaultRating")
    public ResponseEntity<Rating> getDefaultRatingId() {
        return ResponseEntity.ok(ratingModelingService.getDefaultRating());
    }

    @GetMapping("setDefaultRating/{ratingId}")
    public void setDefaultRatingId(@PathVariable String ratingId) {
        this.ratingModelingService.setDefaultRating(ratingId);
    }

    @DeleteMapping("delete/{ratingId}")
    public void delete(@PathVariable String ratingId) {
        this.ratingModelingService.delete(ratingId);
    }
}
