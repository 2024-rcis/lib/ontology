package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.controller;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Process;
import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsProcess;
import de.ubt.ai4.petter.recpro.lib.bpms.modeling.service.BpmsModelingProcessService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmProcessService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/bpm/process/")
@AllArgsConstructor
public class OntologyModelingBpmProcessController {

    private OntologyModelingBpmProcessService ontologyProcessService;
    private BpmsModelingProcessService bpmsProcessService;

    @GetMapping("createAll")
    public void createAll() {
        List<BpmsProcess> processes = bpmsProcessService.getAll();
        ontologyProcessService.createProcesses(Process.fromBpmsProcesses(processes));
    }

    @GetMapping("getAll")
    public ResponseEntity<List<Process>> getAll() {
        return ResponseEntity.ok(ontologyProcessService.getAll());
    }

    @GetMapping("getById/{processId}")
    public ResponseEntity<Process> getById(@PathVariable String processId) {
        return ResponseEntity.ok(ontologyProcessService.getById(processId));
    }

    @GetMapping("getByActivityId/{activityId}")
    public ResponseEntity<List<Process>> getByActivityId(@PathVariable String activityId) {
        return ResponseEntity.ok(ontologyProcessService.getByActivityId(activityId));
    }

    @GetMapping("getByAttributeId/{attributeId}")
    public ResponseEntity<List<Process>> getByAttributeId(@PathVariable String attributeId) {
        return ResponseEntity.ok(ontologyProcessService.getByAttributeId(attributeId));
    }

    @GetMapping("getLatest")
    public ResponseEntity<List<Process>> getLatest() {
        return ResponseEntity.ok(ontologyProcessService.getLatest());
    }

}
