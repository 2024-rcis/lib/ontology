package de.ubt.ai4.petter.recpro.lib.ontology.util;

public final class OntologyFilterIds {
    public static final String FILTER = "Filter";
    public static final String FILTER_PROCESS_AWARE = "ProcessAware";
    public static final String FILTER_BASE = "Base";
    public static final String FILTER_COLLABORATIVE = "Collaborative";
    public static final String FILTER_CONTENT_BASED = "ContentBased";
    public static final String FILTER_HYBRID = "Hybrid";
    public static final String FILTER_KNOWLEDGE_BASED = "KnowledgeBased";

    public static final String DATA_PROPERTY_FILTER_DESCRIPTION = "filterDescription";
    public static final String DATA_PROPERTY_FILTER_ID = "filterId";
    public static final String DATA_PROPERTY_FILTER_NAME = "filterName";
    public static final String DATA_PROPERTY_FILTER_TYPE = "filterType";
    public static final String DATA_PROPERTY_FILTER_URL = "filterUrl";
    public static final String DATA_PROPERTY_FILTER_ALL_INPUT_ATTRIBUTES = "allInputAttributes";
    public static final String DATA_PROPERTY_FILTER_ALL_INPUT_ELEMENTS = "allInputElements";
    public static final String DATA_PROPERTY_FILTER_ALL_INPUT_USERS = "allInputUsers";
    public static final String DATA_PROPERTY_FILTER_ALL_INPUT_RATINGS = "allInputRatings";
    public static final String DATA_PROPERTY_FILTER_TRACE_LENGTH = "traceLength";


    public static final String DATA_PROPERTY_FILTER_TASKLIST_ASCENDING = "tasklistAscending";
    public static final String DATA_PROPERTY_FILTER_TASKLIST_ORDER = "tasklistOrder";

    public static final String OBJECT_PROPERTY_FILTER_USES_ATTRIBUTE = "filterUsesAttribute";
    public static final String OBJECT_PROPERTY_FILTER_USES_BPM_ELEMENT = "filterUsesBpmElement";
    public static final String OBJECT_PROPERTY_FILTER_USES_RATING = "filterUsesRating";
    public static final String OBJECT_PROPERTY_FILTER_USES_USER = "filterUsesUser";
}
