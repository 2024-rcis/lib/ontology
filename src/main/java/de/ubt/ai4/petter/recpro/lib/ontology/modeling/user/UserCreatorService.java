package de.ubt.ai4.petter.recpro.lib.ontology.modeling.user;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.RoleCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.BASE_URL;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyUserIds.*;

@Service
@AllArgsConstructor
public class UserCreatorService {
    public static List<User> fromIterator(ResIterator iterator, OntModel model) {
        List<User> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Resource userInstance = iterator.nextResource();
            result.add(fromResource(userInstance, model));
        }
        return result;
    }

    public static List<User> getListFromResource(Resource resource, OntModel model, String uri) {
        Property property = model.getProperty(BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);
        List<User> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource element = stmt.getObject().asResource();
            User user = fromResource(element, model);
            if (!user.getId().isEmpty() && !user.getId().isBlank()) {
                result.add(fromResource(element, model));
            }
        }
        return result;
    }

    public static User fromResource(Resource resource, OntModel model) {
        User result = new User();
        result.setId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_USER_HAS_ID));
        result.setDescription(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_USER_HAS_DESCRIPTION));
        result.setFirstName(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_USER_HAS_FIRST_NAME));
        result.setLastName(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_USER_HAS_LAST_NAME));
        result.setRoles(RoleCreatorService.rolesFromResource(resource, model, OBJECT_PROPERTY_USER_HAS_ROLE));
        return result;
    }
}
