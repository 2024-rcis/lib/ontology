package de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating;

import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Scale;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class RatingScaleModelingService {
    private RatingScaleDetailModelingService ratingScaleDetailModelingService;

    public OntModel createRatingScale(Scale scale, Individual ratingIndividual, OntModel model) {
        if (scale.getId().isEmpty()) {
            scale.setId(UUID.randomUUID().toString());
        }

        Individual scaleIndividual = ModelingService.getIndividual(model, RATING_SCALE, scale.getId());
        this.createDataProperties(scaleIndividual, model, scale);
        this.createObjectProperties(model, ratingIndividual, scaleIndividual);

        ratingScaleDetailModelingService.createRatingScaleDetail(scale.getDetails(), scaleIndividual, model);

        return model;
    }

    private void createDataProperties(Individual individual, OntModel model, Scale scale) {
        ModelingService.addDatatypeProperty(model, DATA_PROPERTY_RATING_SCALE_ID, individual, scale.getId());
    }

    private void createObjectProperties(OntModel model, Individual ratingIndividual, Individual scaleIndividual) {
        ObjectProperty ratingHasScale = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_RATING_HAS_RATING_SCALE);

        ModelingService.setProperty(ratingIndividual, ratingHasScale, scaleIndividual);
    }
}
