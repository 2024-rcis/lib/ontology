package de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.*;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyAttributeIds.*;
import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyBaseIds.*;

@Service
@AllArgsConstructor
public class AttributeCreatorService {
    public static List<RecproAttribute> fromIterator(ResIterator iterator, OntModel model) {
        List<RecproAttribute> result = new ArrayList<>();
        while(iterator.hasNext()) {
            Resource attributeInstance = iterator.nextResource();
            result.add(fromResource(attributeInstance, model));
        }
        return result;
    }

    public static List<RecproAttribute> fromIterator(ExtendedIterator<Resource> iterator, OntModel model) {
        List<RecproAttribute> result = new ArrayList<>();
        while(iterator.hasNext()) {
            Resource attributeInstance = iterator.next();
            result.add(fromResource(attributeInstance, model));
        }
        return result;
    }

    public static RecproAttribute fromResource(Resource resource, OntModel model) {
        RecproAttribute result = new RecproAttribute();
        initialize(result, resource, model);
        return switch (result.getAttributeType()) {
            case BINARY -> AttributeCreatorService.fromBinaryResource(resource, model);
            case META -> AttributeCreatorService.fromMetaResource(resource, model);
            case TEXT -> AttributeCreatorService.fromTextualResource(resource, model);
            case NUMERIC -> AttributeCreatorService.fromNumericResource(resource, model);
            default -> result;
        };
    }

    public static RecproBinaryAttribute fromBinaryResource(Resource resource, OntModel model) {
        RecproBinaryAttribute attribute = new RecproBinaryAttribute();
        initialize(attribute, resource, model);
        attribute.setDefaultValue(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_BINARY_ATTRIBUTE_DEFAULT_VALUE)));
        return attribute;
    }

    public static RecproMetaAttribute fromMetaResource(Resource resource, OntModel model) {
        RecproMetaAttribute attribute = new RecproMetaAttribute();
        initialize(attribute, resource, model);
        return attribute;
    }

    public static RecproNumericAttribute fromNumericResource(Resource resource, OntModel model) {
        RecproNumericAttribute attribute = new RecproNumericAttribute();
        initialize(attribute, resource, model);
        attribute.setDefaultValue(Double.parseDouble(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_NUMERICAL_ATTRIBUTE_DEFAULT_VALUE)));
        return attribute;
    }

    public static RecproTextualAttribute fromTextualResource(Resource resource, OntModel model) {
        RecproTextualAttribute attribute = new RecproTextualAttribute();
        initialize(attribute, resource, model);
        attribute.setDefaultValue(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_TEXTUAL_ATTRIBUTE_DEFAULT_VALUE));
        return attribute;
    }

    private static void initialize(RecproAttribute attribute, Resource resource, OntModel model) {
        attribute.setId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_ID));
        attribute.setAttributeType(RecproAttributeType.findType(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_TYPE)));
        attribute.setName(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_NAME));
        attribute.setDescription(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_DESCRIPTION));
        attribute.setBpmsAttributeId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_BPMS_ID));
        attribute.setFromUrl(Boolean.parseBoolean(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_FROM_URL)));
        attribute.setUrl(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_ATTRIBUTE_URL));
    }

    public static List<RecproAttribute> getListFromResource(Resource resource, OntModel model, String uri) {
        Property property = model.getProperty(BASE_URL + uri);
        StmtIterator iterator = model.listStatements(resource, property, (RDFNode) null);
        List<RecproAttribute> result = new ArrayList<>();
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Resource attribute = stmt.getObject().asResource();
            result.add(fromResource(attribute, model));
        }
        return result;
    }
}
