package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.util;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute.AttributeCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute.AttributeModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.ElementCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmElementService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating.RatingCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.rating.RatingModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.user.UserCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.user.UserModelingService;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Rating;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyFilterIds.*;

@Service
@AllArgsConstructor
public class FilterCreatorUtil {
    private AttributeModelingService attributeModelingService;
    private OntologyModelingBpmElementService bpmElementModelingService;
    private RatingModelingService ratingModelingService;
    private UserModelingService userModelingService;

    public void initialize(Filter filter, Resource resource, OntModel model) {
        filter.setId(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_ID));
        filter.setName(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_NAME));
        filter.setDescription(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_DESCRIPTION));
        filter.setFilterUrl(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_URL));
        filter.setFilterType(FilterType.valueOf(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_FILTER_TYPE)));
    }

    public List<RecproAttribute> setAttributes(boolean allAttributes, Resource resource, OntModel model) {
        return allAttributes ? attributeModelingService.getAll() : AttributeCreatorService.getListFromResource(resource, model, OBJECT_PROPERTY_FILTER_USES_ATTRIBUTE);
    }

    public List<BpmElement> setElements(boolean allElements, Resource resource, OntModel model) {
        return allElements ? bpmElementModelingService.getAll() : ElementCreatorService.getListFromResource(resource, model, OBJECT_PROPERTY_FILTER_USES_BPM_ELEMENT);
    }

    public List<Rating> setRatings(boolean allRatings, Resource resource, OntModel model) {
        return allRatings ? ratingModelingService.getAll() : RatingCreatorService.getListFromResource(resource, model, OBJECT_PROPERTY_FILTER_USES_RATING);
    }

    public List<User> setUsers(boolean allUsers, Resource resource, OntModel model) {
        return allUsers ? userModelingService.getAll() : UserCreatorService.getListFromResource(resource, model, OBJECT_PROPERTY_FILTER_USES_USER);
    }
}
