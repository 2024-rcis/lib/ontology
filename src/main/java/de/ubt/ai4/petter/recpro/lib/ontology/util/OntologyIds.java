package de.ubt.ai4.petter.recpro.lib.ontology.util;

public final class OntologyIds {
    public static final String ACTIVITY = "Activity";
    public static final String RECPRO_ELEMENT = "BpmElement";


    public static final String PERSPECTIVE = "Perspective";
    public static final String PROCESS = "Process";
    public static final String PROCESS_MODEL = "ProcessModel";
    public static final String RATING = "Rating";
    public static final String ROLE = "Role";

    public static final String OBJECT_PROPERTY_ACTIVITY_HAS_ROLE = "activityHasRole";

    public static final String OBJECT_PROPERTY_PROCESS_HAS_ACTIVITY = "processHasActivity";
    public static final String OBJECT_PROPERTY_PROCESS_HAS_PROCESS_MODEL = "processHasProcessModel";
    public static final String OBJECT_PROPERTY_PROCESS_MODEL_HAS_ACTIVITY = "processModelHasActivity";
    public static final String OBJECT_PROPERTY_PROCESS_MODEL_BELONGS_TO_PROCESS = "processModelBelongsToProcess";
    public static final String OBJECT_PROPERTY_PROCESS_MODEL_HAS_ROLE = "processModelHasRole";
    public static final String OBJECT_PROPERTY_ROLE_HAS_CHILD = "roleHasChild";
    public static final String OBJECT_PROPERTY_ROLE_HAS_PARENT = "roleHasParent";
    public static final String OBJECT_PROPERTY_ROLE_BELONGS_TO_PROCESS_MODEL = "roleBelongsToProcessModel";
    public static final String OBJECT_PROPERTY_ROLE_BELONGS_TO_ACTIVITY = "roleBelongsToActivity";
    public static final String OBJECT_PROPERTY_ACTIVITY_IS_PART_OF_PROCESS = "activityIsPartOfProcess";
    public static final String OBJECT_PROPERTY_ACTIVITY_IS_PART_OF_PROCESS_MODEL = "activityIsPartOfProcessModel";
    public static final String OBJECT_PROPERTY_RECPRO_ELEMENT_HAS_ATTRIBUTE = "bpmElementHasAttribute";
    public static final String DATA_PROPERTY_PROCESS_VERSION = "processVersion";
    public static final String DATA_PROPERTY_PROCESS_CATEGORY = "processCategory";
    public static final String DATA_PROPERTY_PROCESS_KEY = "processKey";
    public static final String DATA_PROPERTY_PROCESS_IS_LATEST = "processIsLatest";
    public static final String DATA_PROPERTY_PROCESS_MODEL_XML = "processModelXml";
    public static final String DATA_PROPERTY_RATING_VALUE = "ratingValue";
    public static final String DATA_PROPERTY_ROLE_ID = "roleId";
    public static final String DATA_PROPERTY_ROLE_NAME = "roleName";
    public static final String DATA_PROPERTY_RECPRO_ELEMENT_ID = "bpmElementId";
    public static final String DATA_PROPERTY_RECPRO_ELEMENT_NAME = "bpmElementName";
    public static final String DATA_PROPERTY_RECPRO_ELEMENT_DESCRIPTION = "bpmElementDescription";
    public static final String DATA_PROPERTY_RECPRO_ELEMENT_TYPE = "bpmElementType";


    public static final String RATING_BINARY_RATING = "BinaryRating";
    public static final String RATING_CONTINUOUS_RATING = "ContinuousRating";
    public static final String RATING_INTERVAL_BASED_RATING = "IntervalBasedRating";
    public static final String RATING_ORDINAL_RATING = "OrdinalRating";
    public static final String RATING_UNARY_RATING = "UnaryRating";
    public static final String OBJECT_PROPERTY_RATING_HAS_RATING_SCALE = "ratingHasRatingScale";
    public static final String OBJECT_PROPERTY_RATING_CONTINUOUS_RATING_HAS_RATING_SCALE = "continuousRatingHasRatingScale";
    public static final String OBJECT_PROPERTY_RATING_INTERVAL_BASED_RATING_HAS_RATING_SCALE = "intervalBasedRatingHasRatingScale";
    public static final String OBJECT_PROPERTY_RATING_ORDINAL_RATING_HAS_RATING_SCALE = "ordinalRatingHasRatingScale";
    public static final String OBJECT_PROPERTY_RATING_IS_PART_OF_BPM_ELEMENT = "ratingIsPartOfBpmElement";
    public static final String OBJECT_PROPERTY_BPM_ELEMENT_HAS_RATING = "bpmElementHasRating";

    public static final String DATA_PROPERTY_RATING_ID = "ratingId";
    public static final String DATA_PROPERTY_RATING_NAME = "ratingName";
    public static final String DATA_PROPERTY_RATING_DESCRIPTION = "ratingDescription";
    public static final String DATA_PROPERTY_RATING_TYPE = "ratingType";
    public static final String DATA_PROPERTY_RATING_BINARY_RATING_DEFAULT_VALUE = "binaryRatingDefaultValue";
    public static final String DATA_PROPERTY_RATING_BINARY_RATING_FALSE_LABEL = "binaryRatingFalseLabel";
    public static final String DATA_PROPERTY_RATING_BINARY_RATING_TRUE_LABEL = "binaryRatingTrueLabel";
    public static final String DATA_PROPERTY_RATING_CONTINUOUS_RATING_DEFAULT_VALUE = "continuousRatingDefaultValue";
    public static final String DATA_PROPERTY_RATING_INTERVAL_BASED_RATING_DEFAULT_VALUE = "intervalBasedRatingDefaultValue";
    public static final String DATA_PROPERTY_RATING_ORDINAL_RATING_DEFAULT_VALUE = "ordinalRatingDefaultValue";
    public static final String DATA_PROPERTY_RATING_UNARY_RATING_DEFAULT_VALUE = "unaryRatingDefaultValue";
    public static final String DATA_PROPERTY_RATING_UNARY_RATING_LABEL = "unaryRatingLabel";

    public static final String RATING_SCALE = "RatingScale";
    public static final String DATA_PROPERTY_RATING_SCALE_ID = "ratingScaleId";
    public static final String RATING_SCALE_DETAIL = "RatingScaleDetail";
    public static final String OBJECT_PROPERTY_RATING_SCALE_HAS_SCALE_DETAIL = "ratingScaleHasScaleDetail";
    public static final String DATA_PROPERTY_RATING_SCALE_DETAIL_ID = "ratingScaleDetailId";
    public static final String DATA_PROPERTY_RATING_SCALE_DETAIL_LABEL = "ratingScaleDetailLabel";
    public static final String DATA_PROPERTY_RATING_SCALE_DETAIL_POSITION = "ratingScaleDetailPosition";
    public static final String DATA_PROPERTY_RATING_SCALE_DETAIL_VALUE = "ratingScaleDetailValue";
}
