package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.controller;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmActivityService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/bpm/activity/")
@AllArgsConstructor
public class OntologyModelingBpmActivityController {

    private OntologyModelingBpmActivityService activityService;

    @GetMapping("getAll")
    public ResponseEntity<List<Activity>> getAll() {
        return ResponseEntity.ok(activityService.getAll());
    }

    @GetMapping("getById/{activityId}")
    public ResponseEntity<Activity> getById(@PathVariable String activityId) {
        return ResponseEntity.ok(activityService.getById(activityId));
    }
}
