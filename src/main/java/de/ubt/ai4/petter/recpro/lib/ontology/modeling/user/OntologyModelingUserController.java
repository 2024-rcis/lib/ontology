package de.ubt.ai4.petter.recpro.lib.ontology.modeling.user;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/user/")
@AllArgsConstructor
public class OntologyModelingUserController {

    private UserModelingService userModelingService;

    @GetMapping("getAll")
    public ResponseEntity<List<User>> getAll() {
        return ResponseEntity.ok(userModelingService.getAll());
    }

    @PostMapping("create")
    public void create(@RequestBody User user) {
        userModelingService.createUser(user);
    }

    @GetMapping("getById/{userId}")
    public ResponseEntity<User> getById(@PathVariable String userId) {
        return ResponseEntity.ok(userModelingService.getById(userId));
    }

    @DeleteMapping("delete/{userId}")
    public void delete(@PathVariable String userId) {
        this.userModelingService.delete(userId);
    }
}
