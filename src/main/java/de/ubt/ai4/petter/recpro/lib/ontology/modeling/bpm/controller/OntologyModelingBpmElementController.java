package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.controller;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmElementService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/bpm/element/")
@AllArgsConstructor
public class OntologyModelingBpmElementController {
    private OntologyModelingBpmElementService modelingService;

    @GetMapping("getAll")
    public ResponseEntity<List<BpmElement>> getAll() {
        return ResponseEntity.ok(modelingService.getAll());
    }
}
