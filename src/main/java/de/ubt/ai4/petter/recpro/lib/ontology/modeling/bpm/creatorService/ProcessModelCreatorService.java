package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.ProcessModel;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.springframework.stereotype.Service;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class ProcessModelCreatorService {
    public static ProcessModel processModelFromResource(Resource resource, OntModel model) {
        ProcessModel result = new ProcessModel();
        ElementCreatorService.initialize(result, resource, model);
        result.setXml(ModelingService.getStringPropertyFromResource(resource, model, DATA_PROPERTY_PROCESS_MODEL_XML));
        result.setActivities(ActivityCreatorService.activitiesFromResource(resource, model, OBJECT_PROPERTY_PROCESS_MODEL_HAS_ACTIVITY));
        result.setRoles(RoleCreatorService.rolesFromResource(resource, model, OBJECT_PROPERTY_PROCESS_MODEL_HAS_ROLE));
        return result;
    }

    public static ProcessModel processModelFromStatement(Statement statement, OntModel model) {
        Resource resource = statement.getResource();
        return processModelFromResource(resource, model);
    }
}
