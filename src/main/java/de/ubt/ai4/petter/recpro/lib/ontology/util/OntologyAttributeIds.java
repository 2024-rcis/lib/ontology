package de.ubt.ai4.petter.recpro.lib.ontology.util;

public final class OntologyAttributeIds {

    public static final String ATTRIBUTE = "Attribute";
    public static final String ATTRIBUTE_BINARY_ATTRIBUTE = "BinaryAttribute";
    public static final String ATTRIBUTE_META_ATTRIBUTE = "MetaAttribute";
    public static final String ATTRIBUTE_CATEGORICAL_ATTRIBUTE = "CategoricalAttribute";
    public static final String ATTRIBUTE_CONTINUOUS_ATTRIBUTE = "ContinuousAttribute";
    public static final String ATTRIBUTE_DISCRETE_ATTRIBUTE = "DiscreteAttribute";
    public static final String ATTRIBUTE_GEOGRAPHICAL_ATTRIBUTE = "GeographicalAttribute";
    public static final String ATTRIBUTE_NUMERICAL_ATTRIBUTE = "NumericalAttribute";
    public static final String ATTRIBUTE_ORDINAL_ATTRIBUTE = "OrdinalAttribute";
    public static final String ATTRIBUTE_RATIO_ATTRIBUTE = "RatioAttribute";
    public static final String ATTRIBUTE_TAXONOMY_ATTRIBUTE = "TaxonomyAttribute";
    public static final String ATTRIBUTE_TEXTUAL_ATTRIBUTE = "TextualAttribute";
    public static final String ATTRIBUTE_TIME_ATTRIBUTE = "TimeAttribute";
    public static final String OBJECT_PROPERTY_ATTRIBUTE_IS_PART_OF_RECPRO_ELEMENT = "attributeIsPartOfBpmElement";
    public static final String OBJECT_PROPERTY_ATTRIBUTE_HAS_ATTRIBUTE = "attributeHasAttribute";
    public static final String DATA_PROPERTY_ATTRIBUTE_ID = "attributeId";
    public static final String DATA_PROPERTY_ATTRIBUTE_BPMS_ID = "bpmsAttributeId";
    public static final String DATA_PROPERTY_ATTRIBUTE_TYPE = "attributeType";
    public static final String DATA_PROPERTY_ATTRIBUTE_VALUE = "attributeValue";
    public static final String DATA_PROPERTY_ATTRIBUTE_NAME = "attributeName";
    public static final String DATA_PROPERTY_ATTRIBUTE_DESCRIPTION = "attributeDescription";
    public static final String DATA_PROPERTY_ATTRIBUTE_FILTERABLE = "attributeFilterable";
    public static final String DATA_PROPERTY_ATTRIBUTE_FROM_URL = "attributeFromUrl";
    public static final String DATA_PROPERTY_ATTRIBUTE_URL = "attributeUrl";
    public static final String DATA_PROPERTY_ATTRIBUTE_BINARY_ATTRIBUTE_TRUE_VALUE = "binaryAttributeTrueValue";
    public static final String DATA_PROPERTY_ATTRIBUTE_BINARY_ATTRIBUTE_FALSE_VALUE = "binaryAttributeFalseValue";
    public static final String DATA_PROPERTY_ATTRIBUTE_BINARY_ATTRIBUTE_DEFAULT_VALUE = "binaryAttributeDefaultValue";
    public static final String DATA_PROPERTY_ATTRIBUTE_TEXTUAL_ATTRIBUTE_DEFAULT_VALUE = "textualAttributeDefaultValue";
    public static final String DATA_PROPERTY_ATTRIBUTE_NUMERICAL_ATTRIBUTE_DEFAULT_VALUE = "numericalAttributeDefaultValue";
}
