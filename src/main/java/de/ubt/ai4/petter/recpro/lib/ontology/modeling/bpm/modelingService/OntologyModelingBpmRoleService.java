package de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Role;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.creatorService.RoleCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.service.ModelingService;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.springframework.stereotype.Service;

import java.util.List;

import static de.ubt.ai4.petter.recpro.lib.ontology.util.OntologyIds.*;

@AllArgsConstructor
@Service
public class OntologyModelingBpmRoleService {
    private OntologyModelingBpmElementService elementService;

    public List<BpmElement> getAll(OntModel model) {
        ResIterator individuals = ModelingService.getResIterator(model, ROLE);
        return RoleCreatorService.fromResIterator(individuals, model);
    }

    public void createRoles(List<Role> roles, OntModel model, Individual individual, boolean isActivity) {
        roles.forEach(recproRole -> createRole(recproRole, model, individual, null, isActivity));
    }

    public void createRoles(List<Role> roles, OntModel model, Individual individual, Individual parent, boolean isActivity) {
        roles.forEach(recproRole -> createRole(recproRole, model, individual, parent, isActivity));
    }

    public void createRole(Role role, OntModel model, Individual individual, Individual parent, boolean isActivity) {
        Individual roleIndividual = ModelingService.getIndividual(model, ROLE, role.getId());
        this.createDataProperties(model, roleIndividual, role, individual, isActivity);
        this.createObjectProperties(model, roleIndividual, individual, parent, isActivity);
    }

    private void createDataProperties(OntModel model, Individual individual, Role role, Individual parent, boolean isActivity) {
        elementService.createRecproElementIndividual(role, individual, model);
        if (!role.getChildElements().isEmpty()) {
            createRoles(role.getChildElements(), model, individual, parent, isActivity);
        }
    }

    private void createObjectProperties(OntModel model, Individual roleIndividual, Individual individual, Individual parent, boolean isActivity) {
        if (isActivity) {
            ObjectProperty roleBelongsToActivity = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ROLE_BELONGS_TO_ACTIVITY);
            ObjectProperty activityHasRole = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ACTIVITY_HAS_ROLE);

            ModelingService.addProperty(roleIndividual, roleBelongsToActivity, individual);
            ModelingService.addProperty(individual, activityHasRole, roleIndividual);
        } else {
            ObjectProperty roleBelongsToProcessModel = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ROLE_BELONGS_TO_PROCESS_MODEL);
            ObjectProperty processModelHasRole = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_PROCESS_MODEL_HAS_ROLE);
            ModelingService.addProperty(roleIndividual, roleBelongsToProcessModel, individual);
            ModelingService.addProperty(individual, processModelHasRole, roleIndividual);
        }

        if (parent != null) {
            ObjectProperty roleHasChild = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ROLE_HAS_CHILD);
            ObjectProperty roleHasParent = ModelingService.getObjectPropertyFromModel(model, OBJECT_PROPERTY_ROLE_HAS_PARENT);

            ModelingService.addProperty(roleIndividual, roleHasParent, individual);
            ModelingService.addProperty(individual, roleHasChild, roleIndividual);
        }

    }

}
