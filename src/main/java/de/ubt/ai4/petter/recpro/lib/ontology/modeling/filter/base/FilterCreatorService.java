package de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.collaborative.CollaborativeFilterCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.contentBased.ContentBasedFilterCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.knowledgeBased.KnowledgeBasedFilterCreatorService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.util.FilterCreatorUtil;
import lombok.AllArgsConstructor;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class FilterCreatorService {

    private FilterCreatorUtil filterCreatorUtil;
    private BaseFilterCreatorService baseFilterCreatorService;
    private CollaborativeFilterCreatorService collaborativeFilterCreatorService;
    private ContentBasedFilterCreatorService contentBasedFilterCreatorService;

    private KnowledgeBasedFilterCreatorService knowledgeBasedFilterCreatorService;
    public Filter fromResource(Resource resource, OntModel model) {
        Filter res = new Filter();
        filterCreatorUtil.initialize(res, resource, model);

        return switch (res.getFilterType()) {
            case KNOWLEDGE_BASED -> knowledgeBasedFilterCreatorService.fromResource(resource, model);
            case BASE -> baseFilterCreatorService.fromResource(resource, model);
            case COLLABORATIVE -> collaborativeFilterCreatorService.fromResource(resource, model);
            case CONTENT_BASED -> contentBasedFilterCreatorService.fromResource(resource, model);
            default ->  res;
        };
    }

    public List<Filter> fromIterator(ResIterator iterator, OntModel model) {
        List<Filter> result = new ArrayList<>();
        while(iterator.hasNext()) {
            Resource filterInstance = iterator.nextResource();
            result.add(fromResource(filterInstance, model));
        }
        return result;
    }
}
